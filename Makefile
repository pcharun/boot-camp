default:
	@echo "###################-Bootcamp Solution MakeFile-##########################"
	@echo "- build: Build the service"
	@echo "- devshell: Run the container and open a sh shell inside it"
	@echo "- up: Run the container and executes the service inside"
	@echo "- down: Take down the service container and all the dependent containers"
	@echo "###################-Bootcamp Solution MakeFile-##########################"

build:
	@docker-compose build --no-cache

devshell:
	@docker-compose run --rm --service-ports api sh

up:
	@docker-compose run --rm --service-ports api

down:
	@docker-compose down --remove-orphans