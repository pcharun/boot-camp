# Boot Camp
This Project define basic definition to create a shoping cart to will be used into training backend development team

## **Routes into the project.**

>## **List all available products**
### **PATH**
>/products 
### **METHOD**
>  GET 
### **Description**
    List all available products 
### **Params**
    -none- 
### **Body**
    -none-
### **RESPONSES**
* **200**

 ```
{ 
    data: [{
        id: int, 
        name: string, 
        price: decimal 
    }]
}
 ```
 * **500**
```
{
    msg: string
}
```

>## **Created shopping Cart**
### **PATH**
> /cart 
### **METHOD**
> POST
### **Description**
    Created shoping cart 
### **Params** 
    -- none --
### **Body**
    -- none --
### **RESPONSES**
* **200**
``` 
{ 
    id: string, 
    items: [{ 
        id: int, quantity: int, 
    }] 
} 
```
* **500**
```
{
    msg: string
}
```
> ## **Add a particular product a particular shoping cart**
### **PATH**
> /cart/{cartID} 
### **METHOD**
> PATCH
### **Description**
Add a particular product a particular shoping cart 
### **Params** 
- cartID: integer, cart code
### **Body**
```
{
    items: [{
        id: int, 
        quantity: int
    }]
} 
``` 
### **RESPONSES**
* **200**
```
{ 
    id: string, 
    items: [{ 
        id: int, 
        quantity: int
    }]
} 
``` 
* **404**
```
{
    msg: string
}
```
Example:
```
{
    msg: "Cart not Found"
}
```
* **500**
```
{
    msg: string
}
```

>## **Modify the amount of a particular shoping cart**
### **PATH**
>/cart/{cartID}/item/{itemID} 
### **METHOD**
>PATCH 
### **Description**
>Modify the amount of a particular shoping cart 
### **Params** 
- cartID: integer, cart code.
- itemID: integer, product code.
### **Body**
``` 
{
    items: [{
        id: int, 
        quantity: int
    }]
} 
``` 
### **RESPONSES**
* **200**
```
{
    id: string,
    items: [{
        id: int,
        quantity: int
    }]
} 
``` 
* **404**
```
{
    msg: string
}
```
Examples:
```
{
    msg: "Cart not found"
}
```
```
{
    msg: "Product not found"
}
```
* **500**
```
{
    msg: string
}
```

>## **Delete a particular product in a particular shoping cart**
### **PATH**
>/cart/{cartID}/item/{itemID} 
### **METHOD**
>DELETE 
### **Description**
Delete a particular product in a particular shoping cart 
### **Params**
- cartID: integer, cart code.
- itemID: integer, product code.
### **Body**
``` 
--none-- 
```
### **RESPONSES**
* **200**
```
{ 
    id: string, 
    items: [{ 
        id: int, 
        quantity: int
    }]
}
```
* **404**
```
{
    msg: string
}
```
Examples:
```
{
    msg: "Cart not found"
}
```
```
{
    msg: "Product not found"
}
```
* **500**
```
{
    msg: string
}
```

>## **Delete all products from a particular shoping cart**
### **PATH**
>/cart/{cartID} 
### **METHOD**
>PUT
### **Description**
Delete all products from a particular shoping cart
### **Params** 
- cartID: Integer: cart code
### **Body**
    -- none --
### **RESPONSES**
* **200**
```
{ 
    id: string,
    items: [{ 
        id: int,
        quantity: int
    }]
}
```
* **404**
```
{
    msg: string
}
```
Examples:
```
{
    msg: "Cart not found"
}
```
* **500**
```
{
    msg: string
}
```

>## **Delete a particular shoping cart entirely**
### **PATH**
>/cart/{cartID}
### **METHOD**
>DELETE 
### **Description**
Delete a particular shoping cart entirely 
### **Params** 
### **Body**
### **RESPONSES**
* **204** 
```
    null
```
* **404**
```
{
    msg: string
}
```
Examples:
```
{
    msg: "Cart not found"
}
```
* **500**
```
{
    msg: string
}
```