package product

import (
	"time"

	"gitlab.com/pcharun/boot-camp/pkg/model"
)

type productResponse struct {
	Meta map[string]string `json:"meta,omitempty"`
	Data []model.Product   `json:"data,omitempty"`
}

type resultQuery struct {
	UrlRequest  string          `bson:"url_request"`
	ExpiredTime time.Time       `bson:"expired_time"`
	Data        []model.Product `bson:"data"`
}
