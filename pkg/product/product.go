package product

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/pcharun/boot-camp/pkg/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const COLLECTION_NAME = "product"
const URL_API = "https://bootcamp-products.getsandbox.com"
const time_expiration_mins = 5

type ProductServiceInterface interface {
	ListAllProducts(page, limit int) ([]model.Product, error)
	GetDetailProduct(id int) (model.Product, error)
}

func NewProductService(db *mongo.Database) ProductServiceInterface {
	return &productService{
		collection: db.Collection(COLLECTION_NAME),
	}
}

type productService struct {
	collection *mongo.Collection
}

func (ps *productService) ListAllProducts(page, limit int) ([]model.Product, error) {
	url_request := URL_API + "/products"
	timeNow := time.Now()
	filters := bson.M{
		"url_requesst": url_request,
		"expired_time": bson.M{"$gte": primitive.NewDateTimeFromTime(timeNow)},
	}
	var result resultQuery
	err := ps.collection.FindOne(context.Background(), filters).Decode(&result)

	if err != nil {
		resp, err := http.Get(url_request)
		if err != nil {
			return []model.Product{}, err
		}
		var pr productResponse
		if err := json.NewDecoder(resp.Body).Decode(&pr); err != nil {
			return []model.Product{}, err
		}
		result.UrlRequest = url_request
		result.Data = pr.Data
		result.ExpiredTime = timeNow.Add(time.Minute * time_expiration_mins)

		upsr, err := ps.collection.UpdateOne(context.Background(), bson.M{"url_request": url_request}, bson.M{"$set": result})
		if err != nil {
			return []model.Product{}, err
		}
		if upsr.ModifiedCount == 0 {
			_, err := ps.collection.InsertOne(context.Background(), result)
			if err != nil {
				return []model.Product{}, err
			}
		}
	}
	return result.Data, nil
}

func (ps *productService) GetDetailProduct(id int) (model.Product, error) {
	data, err := ps.ListAllProducts(1, 100)
	if err != nil {
		return model.Product{}, err
	}
	for _, v := range data {
		if v.ID == strconv.Itoa(id) {
			return v, nil
		}
	}
	return model.Product{}, errors.New("product not found")
}
