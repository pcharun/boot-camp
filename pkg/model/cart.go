package model

type Cart struct {
	ID       string        `bson:"id,omitempty"`
	OwnerID  string        `bson:"owner_id,omitempty"`
	Products []CartProduct `bson:"products,omitempty"`
}

type CartProduct struct {
	ID       int     `bson:"id,omitempty"`
	Price    float32 `bson:"price,omitempty"`
	Quantity int     `bson:"quantity,omitempty"`
}
