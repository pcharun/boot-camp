package model

type Product struct {
	ID    string  `bson:"id" json:"id,omitempty"`
	Name  string  `bson:"name" json:"name,omitempty"`
	Price float32 `bson:"price" json:"price,string,omitempty"`
}
