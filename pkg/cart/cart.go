package cart

import (
	"context"
	"errors"
	"reflect"

	"gitlab.com/pcharun/boot-camp/pkg/model"
	"gitlab.com/pcharun/boot-camp/pkg/product"
	"gitlab.com/pcharun/boot-camp/viewmodel"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const COLLECTION_NAME = "cart"

type CartServiceInterface interface {
	RegisterNewCart(cart model.Cart) (model.Cart, error)
	GetDetailCart(ownerID string) (model.Cart, error)
	AddProductToCart(ownerID string, item viewmodel.CartItem) (model.Cart, error)
	UpdateProductFromCart(ownerID string, productID int, item viewmodel.CartItem) (model.Cart, error)
	DeleteProductFromCart(ownerID string, itemID int) (model.Cart, error)
	DeleteAllProductFromCart(ownerID string) (model.Cart, error)
	DeleteCart(ownerID string) (model.Cart, error)
}

func NewCartService(db *mongo.Database, ps product.ProductServiceInterface) CartServiceInterface {
	return &cartService{
		collection:     db.Collection(COLLECTION_NAME),
		productService: ps,
	}
}

type cartService struct {
	collection     *mongo.Collection
	productService product.ProductServiceInterface
}

func (cs *cartService) RegisterNewCart(cart model.Cart) (model.Cart, error) {
	m, err := bson.Marshal(cart)
	if err != nil {
		return cart, err
	}

	var mc model.Cart
	filters := bson.M{
		"owner_id": cart.OwnerID,
	}
	cs.collection.FindOne(context.Background(), filters).Decode(&mc)
	if !reflect.ValueOf(mc).IsZero() {
		return cart, errors.New("Already exists cart for user")
	}

	res, err := cs.collection.InsertOne(context.Background(), m)
	if err != nil {
		return cart, err
	}
	cart.ID = res.InsertedID.(primitive.ObjectID).Hex()
	cart.Products = make([]model.CartProduct, 1)
	return cart, nil
}

func (cs *cartService) GetDetailCart(ownerID string) (model.Cart, error) {
	var cart model.Cart
	filters := bson.M{
		"owner_id": ownerID,
	}
	err := cs.collection.FindOne(context.Background(), filters).Decode(&cart)
	if err != nil {
		return model.Cart{}, err
	}

	return cart, nil
}

func (cs *cartService) AddProductToCart(ownerID string, item viewmodel.CartItem) (model.Cart, error) {
	var cart model.Cart
	filters := bson.M{
		"owner_id": ownerID,
	}
	err := cs.collection.FindOne(context.Background(), filters).Decode(&cart)
	if err != nil {
		return model.Cart{}, err
	}
	exists := false
	for k, i := range cart.Products {
		if i.ID == item.ID {
			cart.Products[k].Quantity++
			exists = true
		}
	}
	if !exists {
		p, _ := cs.productService.GetDetailProduct(item.ID)
		cart.Products = append(cart.Products, model.CartProduct{
			ID:       item.ID,
			Price:    p.Price,
			Quantity: item.Quantity,
		})
	}
	cs.collection.UpdateOne(context.Background(), filters, bson.M{"$set": cart})
	return cart, nil
}

func (cs *cartService) UpdateProductFromCart(ownerID string, productID int, item viewmodel.CartItem) (model.Cart, error) {
	var cart model.Cart
	filters := bson.M{
		"owner_id": ownerID,
	}
	if item.Quantity <= 0 {
		return cs.DeleteProductFromCart(ownerID, productID)
	}
	err := cs.collection.FindOne(context.Background(), filters).Decode(&cart)
	if err != nil {
		return model.Cart{}, err
	}
	exists := false
	for k, i := range cart.Products {
		if i.ID == item.ID {
			cart.Products[k].Quantity = item.Quantity
			exists = true
		}
	}
	if !exists {
		p, _ := cs.productService.GetDetailProduct(item.ID)
		cart.Products = append(cart.Products, model.CartProduct{
			ID:       item.ID,
			Price:    p.Price,
			Quantity: item.Quantity,
		})
	}
	cs.collection.UpdateOne(context.Background(), filters, bson.M{"$set": cart})
	return cart, nil
}

func (cs *cartService) DeleteProductFromCart(ownerID string, itemID int) (model.Cart, error) {
	var cart model.Cart
	filters := bson.M{
		"owner_id": ownerID,
	}
	err := cs.collection.FindOne(context.Background(), filters).Decode(&cart)
	if err != nil {
		return model.Cart{}, err
	}
	exists := false
	l := len(cart.Products)
	if l == 1 {
		_, err = cs.collection.UpdateOne(context.Background(), filters, bson.M{"$unset": bson.M{"products": 1}})
		if err != nil {
			return model.Cart{}, err
		}
	}
	for k, v := range cart.Products {
		if v.ID == itemID {
			cart.Products = append(cart.Products[:k], cart.Products[k+1:]...)
			exists = true
		}
	}
	if !exists {
		return model.Cart{}, errors.New("product not found")
	}
	cs.collection.UpdateOne(context.Background(), filters, bson.M{"$set": cart})
	return cart, nil
}

func (cs *cartService) DeleteAllProductFromCart(ownerID string) (model.Cart, error) {
	var cart model.Cart
	filters := bson.M{
		"owner_id": ownerID,
	}
	err := cs.collection.FindOne(context.Background(), filters).Decode(&cart)
	if err != nil {
		return model.Cart{}, err
	}
	cart.Products = make([]model.CartProduct, 0)
	_, err = cs.collection.UpdateOne(context.Background(), filters, bson.M{"$unset": bson.M{"products": 1}})
	if err != nil {
		return model.Cart{}, err
	}
	return cart, nil
}

func (cs *cartService) DeleteCart(ownerID string) (model.Cart, error) {
	filters := bson.M{
		"owner_id": ownerID,
	}
	_, err := cs.collection.DeleteOne(context.Background(), filters)
	if err != nil {
		return model.Cart{}, err
	}
	return model.Cart{}, nil
}
