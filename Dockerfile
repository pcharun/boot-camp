# Build Image
FROM golang:1.17.3-alpine3.14 as build

# General setup
ENV SERVICE_NAME=boot-camp
ENV APP_DIR=/go/src/gitlab.com/pcharun/${SERVICE_NAME}
RUN apk add make git build-base
RUN go get github.com/silenceper/gowatch

WORKDIR ${APP_DIR}
COPY . .
RUN go mod tidy
RUN go build

# Deployable Image
FROM alpine:3.12

# Add binary to bin directory
ENV SERVICE_NAME=boot-camp
ENV BUILDER_APP_DIR=/go/src/gitlab.com/pcharun/${SERVICE_NAME}
WORKDIR /app
COPY --from=build ${BUILDER_APP_DIR}/${SERVICE_NAME} .

ENTRYPOINT ["./boot-camp"]