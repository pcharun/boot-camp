package controller

import (
	"encoding/json"
	"net/http"

	"gitlab.com/pcharun/boot-camp/pkg/product"
	"gitlab.com/pcharun/boot-camp/viewmodel"
)

type productController struct {
	Service product.ProductServiceInterface
}

func NewProductController(ps product.ProductServiceInterface) productController {
	return productController{
		Service: ps,
	}
}

func (pc productController) ProductListHandler(w http.ResponseWriter, r *http.Request) {
	page := 1
	limit := 20
	response, err := pc.Service.ListAllProducts(page, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	result, err := json.Marshal(viewmodel.ProductModelToViewmodel(response))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
