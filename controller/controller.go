package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/pcharun/boot-camp/pkg/cart"
	"gitlab.com/pcharun/boot-camp/pkg/model"
	"gitlab.com/pcharun/boot-camp/viewmodel"
)

type CartController struct {
	Service cart.CartServiceInterface
}

func NewCartController(cs cart.CartServiceInterface) CartController {
	return CartController{
		Service: cs,
	}
}
func (cc CartController) CartCreateHandler(w http.ResponseWriter, r *http.Request) {
	jd := json.NewDecoder(r.Body)
	jd.DisallowUnknownFields()

	var c viewmodel.Cart
	err := jd.Decode(&c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	cart, err := cc.Service.RegisterNewCart(viewmodel.CartViewModelToModel(c))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	result, err := json.Marshal(viewmodel.CartModelToViewModel(cart))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func (cc CartController) CartDetailsHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	modelcart, err := cc.Service.GetDetailCart(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := json.Marshal(modelcart)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

func (cc CartController) CartAddProductHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	jd := json.NewDecoder(r.Body)
	var pc viewmodel.Cart
	err := jd.Decode(&pc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var modelcart model.Cart
	for k, v := range pc.Products {
		modelcart, err = cc.Service.AddProductToCart(id, v)
		if err != nil {
			http.Error(w, err.Error()+fmt.Sprintf(", error in item %d", k+1), http.StatusInternalServerError)
			return
		}
	}
	resp, err := json.Marshal(modelcart)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

func (cc CartController) CartUpdateProductAmountHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	productID, err := strconv.Atoi(mux.Vars(r)["productID"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	jd := json.NewDecoder(r.Body)
	var pc viewmodel.Cart
	err = jd.Decode(&pc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var modelcart model.Cart
	for k, v := range pc.Products {
		modelcart, err = cc.Service.UpdateProductFromCart(id, productID, v)
		if err != nil {
			http.Error(w, err.Error()+fmt.Sprintf(", error in item %d", k+1), http.StatusInternalServerError)
			return
		}
	}
	resp, err := json.Marshal(modelcart)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

func (cc CartController) CartDeleteProductHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	productID, err := strconv.Atoi(mux.Vars(r)["productID"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	result, err := cc.Service.DeleteProductFromCart(id, productID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp, err := json.Marshal(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

func (cc CartController) CartDeleteAllProductHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	result, err := cc.Service.DeleteAllProductFromCart(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp, err := json.Marshal(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

func (cc CartController) CartDeleteHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	result, err := cc.Service.DeleteCart(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, err := json.Marshal(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusNoContent)
	w.Write(resp)
}
