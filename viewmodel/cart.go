package viewmodel

import "gitlab.com/pcharun/boot-camp/pkg/model"

type Cart struct {
	ID       string     `json:"id,omitempty"`
	OwnerID  string     `json:"owner_id,omitempty"`
	Products []CartItem `json:"products,omitempty"`
}

type CartItem struct {
	ID       int `json:"id,omitempty"`
	Quantity int `json:"quantity,omitempty"`
}

func CartViewModelToModel(cart Cart) model.Cart {
	return model.Cart{
		ID:      cart.ID,
		OwnerID: cart.OwnerID,
	}
}

func CartModelToViewModel(cart model.Cart) Cart {
	return Cart{
		ID:      cart.ID,
		OwnerID: cart.OwnerID,
	}
}
