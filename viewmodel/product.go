package viewmodel

import (
	"fmt"

	"gitlab.com/pcharun/boot-camp/pkg/model"
)

type Product struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Price string `json:"price"`
}

func ProductModelToViewmodel(products []model.Product) []Product {
	var result []Product
	for _, v := range products {
		result = append(result, Product{
			ID:    v.ID,
			Name:  v.Name,
			Price: fmt.Sprint(v.Price),
		})
	}
	return result
}
