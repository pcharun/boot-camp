package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/pcharun/boot-camp/controller"
	"gitlab.com/pcharun/boot-camp/pkg/cart"
	"gitlab.com/pcharun/boot-camp/pkg/db"
	"gitlab.com/pcharun/boot-camp/pkg/product"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	conn := db.NewConnection(ctx)

	ps := product.NewProductService(conn)
	pctr := controller.NewProductController(ps)

	cs := cart.NewCartService(conn, ps)
	ctr := controller.NewCartController(cs)

	r := mux.NewRouter()
	r.HandleFunc("/cart", ctr.CartCreateHandler).Methods(http.MethodPost)
	r.HandleFunc("/cart/{id}", ctr.CartAddProductHandler).Methods(http.MethodPatch)
	r.HandleFunc("/cart/{id}", ctr.CartDetailsHandler).Methods(http.MethodGet)
	r.HandleFunc("/cart/{id}/item/{productID}", ctr.CartUpdateProductAmountHandler).Methods(http.MethodPatch)
	r.HandleFunc("/cart/{id}", ctr.CartDeleteHandler).Methods(http.MethodDelete)
	r.HandleFunc("/cart/{id}/item", ctr.CartDeleteAllProductHandler).Methods(http.MethodDelete)
	r.HandleFunc("/cart/{id}/item/{productID}", ctr.CartDeleteProductHandler).Methods(http.MethodDelete)
	r.HandleFunc("/product", pctr.ProductListHandler).Methods(http.MethodGet)
	http.Handle("/", r)
	addr := "0.0.0.0:" + os.Getenv("PORT")
	srv := &http.Server{
		Handler:      r,
		Addr:         addr,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	fmt.Println("Starting server.....")
	log.Fatal(srv.ListenAndServe())
}
